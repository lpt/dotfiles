#!/bin/sh

git-pull-repos()
{
  for repo in $1
  do
    cd "$repo"
    echo Rebasing.. $repo
    git co master
    git pull --rebase
    cd >/dev/null -
  done
}

